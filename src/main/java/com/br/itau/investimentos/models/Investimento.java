package com.br.itau.investimentos.models;

import com.br.itau.investimentos.enums.RiscoDoInvestimento;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Investimento{

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        @Column(name = "nome_completo")
        @Size(min = 8, max = 255, message = "O nome deve ter entre 5 e 255 caracteres")
        private String nome;

        @Column(name = "descricao_investimento")
        @Size(min = 0, max = 255, message = "A descricao deve ter até 255 caracteres")
        private String descricao;

        @Column(name = "risco_investimento")
        private RiscoDoInvestimento riscoDoInvestimento;

        @Column(name = "lucro_porcentagem")
        private Double lucroPorcentagem;

        public Investimento(){

        }

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getNome() {
                return nome;
        }

        public void setNome(String nome) {
                this.nome = nome;
        }

        public String getDescricao() {
                return descricao;
        }

        public void setDescricao(String descricao) {
                this.descricao = descricao;
        }

        public RiscoDoInvestimento getRiscoDoInvestimento() {
                return riscoDoInvestimento;
        }

        public void setRiscoDoInvestimento(RiscoDoInvestimento riscoDoInvestimento) {
                this.riscoDoInvestimento = riscoDoInvestimento;
        }

        public Double getLucroPorcentagem() {
                return lucroPorcentagem;
        }

        public void setLucroPorcentagem(Double lucroPorcentagem) {
                this.lucroPorcentagem = lucroPorcentagem;
        }
}