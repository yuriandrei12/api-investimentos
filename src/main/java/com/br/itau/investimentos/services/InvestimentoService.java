package com.br.itau.investimentos.services;

import com.br.itau.investimentos.models.Investimento;
import com.br.itau.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Iterable<Investimento> buscarTodosInvestimentos(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public Optional<Investimento> buscarInvestimentoPorId(Integer id){
        Optional<Investimento> investimento = investimentoRepository.findById(id);
        return investimento;
    }

    public Investimento incluirInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }
}