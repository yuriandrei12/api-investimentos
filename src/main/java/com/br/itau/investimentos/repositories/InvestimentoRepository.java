package com.br.itau.investimentos.repositories;


import com.br.itau.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;


public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {

}