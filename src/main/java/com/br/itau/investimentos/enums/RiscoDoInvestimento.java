package com.br.itau.investimentos.enums;

public enum RiscoDoInvestimento {
    ALTO,
    BAIXO,
    MEDIO
}