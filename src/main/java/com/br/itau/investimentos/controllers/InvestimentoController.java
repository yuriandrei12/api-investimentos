package com.br.itau.investimentos.controllers;

import com.br.itau.investimentos.models.Investimento;
import com.br.itau.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {
    @Autowired
    InvestimentoService investimentoService;

    @GetMapping
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodosInvestimentos();
    }

    @GetMapping("/{id}")
    public Investimento buscarInvestimento(@PathVariable Integer id){
        Optional<Investimento> optionalInvestimento = investimentoService.buscarInvestimentoPorId(id);

        if (optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Investimento> incluirInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento investimentoObjeto = investimentoService.incluirInvestimento(investimento);
        return ResponseEntity.status(201).body(investimentoObjeto);
    }
}